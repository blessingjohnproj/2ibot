class TestSortedDuplicate:
    def sorted_list_number(self):
        numbers = [1, 5, 100, 50, 5, 21, 30, 30, 5, 5]
        'sort the list of number and remove duplicate using set method'
        sorted_number = sorted(list(set(numbers)))
        'print the number in descending order'
        print(list(reversed(sorted_number)))

#Calling the class and creating instance of class
my_number = TestSortedDuplicate()
my_number.sorted_list_number()
